# Gophish Integration

Gophish is a powerful, open-source phishing framework that makes it easy to 
test your organization's exposure to phishing.

This module provides access to the Gophish API via a Service in the 
Service Container.

**Helpful Links:**

* [Project Page](https://www.drupal.org/project/gophish)
* [Module Documentation](https://www.drupal.org/docs/8/modules/gophish-integration)
* [Gophish Project Homepage](https://getgophish.com/)
* [Gophish Documentation](https://getgophish.com/documentation/)

## Installation

The easiest and recommended way to install the module is via composer:

```bash
composer require drupal/gophish
```

For further installation instructions, please refer to the module's 
documentation on Drupal.org: [Installing Gophish Integration](https://www.drupal.org/docs/8/modules/gophish-integration/installing-gophish-integration).

## Configuration

Admin path: /admin/config/gophish

On the modules configuration page, you must add your Gophish host URL, 
and the API Key, for the module to work with your Gophish installation's API.

For further configuration instructions, please refer to the module's: 
[Configuration](https://www.drupal.org/docs/8/modules/gophish-integration/configuration).

## Contributing
Pull requests are welcome. For major changes, please open an issue first 
to discuss what you would like to change.

## License
[GNU General Public License v2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)
