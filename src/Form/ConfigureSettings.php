<?php

namespace Drupal\gophish\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigureSettings.
 *
 * @package Drupal\gophish\Form
 */
class ConfigureSettings extends ConfigFormBase {

  /**
   * Getter method for Form ID.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'gophish_module_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'gophish.config',
    ];
  }

  /**
   * Build the form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gophish.config');

    $form['host'] = [
      '#type' => 'url',
      '#title' => $this->t('Host URL'),
      '#description' => $this->t('The URL of the host for your Gophish installation. Ensure there is no trailing slash, otherwise endpoints will not work.'),
      '#required' => TRUE,
      '#default_value' => $config->get('host'),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('The API Key provided by your Gophish installation.'),
      '#required' => TRUE,
      '#default_value' => $config->get('api_key'),
    ];

    $form['logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Logging'),
      '#description' => $this->t('Enable logging for API calls.'),
      '#default_value' => $config->get('logging'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements form validation.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $host = $form_state->getValue('host');
    if (substr($host, -1) === '/') {
      // Set an error for the form element with a key of "title".
      $form_state->setErrorByName('host', $this->t('The host URL must not end with a trailing slash.'));
    }
  }

  /**
   * Implements a form submit handler.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('gophish.config');
    $config->set('host', $values['host'])->save();
    $config->set('api_key', $values['api_key'])->save();
    $config->set('logging', $values['logging'])->save();
    parent::submitForm($form, $form_state);
  }

}
